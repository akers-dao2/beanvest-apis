## Nestjs Web API

---------
	
A Nestjs Web API solution for Robinhood and Slack integration.

**Installation:**

You need to node and npm. At the root of the project type:

```node
npm install
```

**Create .env Configuration:**
```
USERNAME=
PASSWORD=
TOKEN=
SLACK_TOKEN=
APIKEY=
AUTHDOMAIN=
PROJECTID=
PUBLIC_KEY=
PRIVATE_KEY=
ENCRYPTION_KEY=
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. Nestjs (https://nestjs.com)
1. FireBase (https://www.firebase.com)
1. Slack API (https://www.npmjs.com/package/@slack/web-api)
1. Cheerio (https://cheerio.js.org/)


----------
**Features:**

- APIs to interact with Robinhood and Slack


