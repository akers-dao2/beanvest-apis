import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { QuoteController } from './quote/quote.controller';
import { QuoteService } from './quote/quote.service';
import { RobinhoodController } from './api/robinhood/robinhood.controller';
import { RobinhoodService } from './api/robinhood/robinhood.service';
import { JsonService } from './api/robinhood/json/json.service';
import { AccessTokenService } from './api/robinhood/access-token/access-token.service';
import { FilterOptionsService } from './api/robinhood/filter-options/filter-options.service';
import { EarningsController } from './earnings/earnings.controller';
import { EarningsService } from './earnings/earnings.service';
import { SlackController } from './slack/slack.controller';
import { SlackService } from './slack/slack.service';
import { VerifyRequest } from './shared/middleware/verify-request.middleware';
import { StockAvgPriceController } from './stock-avg-price/stock-avg-price.controller';
import { StockAvgPriceService } from './stock-avg-price/stock-avg-price.service';
import { EncryptionService } from './shared/services/encryption.service';
import { FireService } from './shared/services/firebase.service';
import { UserFromFirebaseService } from './shared/services/get-user-from-firebase.service';
import { WebClientService } from './shared/services/web-client.service';
import { CurrencyFormatterService } from './shared/services/currency_formatter';

@Module({
  imports: [],
  controllers: [AppController, QuoteController, RobinhoodController, EarningsController, SlackController, StockAvgPriceController],
  providers: [AppService,
    QuoteService,
    RobinhoodService,
    JsonService,
    AccessTokenService,
    FilterOptionsService,
    EarningsService,
    SlackService,
    StockAvgPriceService,
    EncryptionService,
    FireService,
    UserFromFirebaseService,
    WebClientService,
    CurrencyFormatterService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(VerifyRequest)
      .forRoutes('slack');
  }
}
