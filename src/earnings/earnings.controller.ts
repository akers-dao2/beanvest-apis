import { Controller, Get, Param, UsePipes } from '@nestjs/common';
import { UppercasePipe } from '../shared/pipes/uppercase.pipe';
import { EarningsService } from './earnings.service';

@Controller('earnings')
export class EarningsController {
    constructor(private earningsService: EarningsService) { }

    @Get(':ticker')
    @UsePipes(new UppercasePipe())
    async earnings(@Param('ticker') ticker: string) {
        return await this.earningsService.get(ticker);
    }
}
