import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as moment from 'moment';
import fetch from 'node-fetch';
import * as cheerio from 'cheerio';

@Injectable()
export class EarningsService {

    async get(ticker: string) {
        try {
            const response = await fetch(`https://www.earningswhispers.com/epsdetails/${ticker}`);
            const htmlText = await response.text();

            const htmlObject = cheerio.load(htmlText);

            const earningsDate = htmlObject('#chartbox').html().split('<br>')[1];
            const isPayable = moment().isBetween(moment(earningsDate, 'MM-DD-YYYY'), moment(earningsDate, 'MM-DD-YYYY').add(10, 'days'));
            return { earningsDate, isPayable: !isPayable };
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
