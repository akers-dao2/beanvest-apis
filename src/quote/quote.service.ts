import { Injectable } from '@nestjs/common';
import * as cheerio from 'cheerio';
import fetch from 'node-fetch';

@Injectable()
export class QuoteService {
    async getQuote(ticker: string) {
        try {
            const response = await fetch(`https://finance.yahoo.com/quote/${ticker}`, { mode: 'no-cors' } as any);
            const htmlText = await response.text();
            const htmlObject = cheerio.load(htmlText);
            const quote = parseFloat(htmlObject('#quote-header-info > div:nth-of-type(3) > div span').slice(0, 1).eq(0).text().replace(',', ''));
            return { quote };
        } catch (e) {
            throw e;
        }
    }
}
