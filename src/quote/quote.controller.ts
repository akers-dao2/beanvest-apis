import { Controller, Get, Param } from '@nestjs/common';
import { QuoteService } from './quote.service';

@Controller('quote')
export class QuoteController {
    constructor(private quote: QuoteService) { }

    @Get(':ticker')
    async find(@Param('ticker') ticker: string) {
        return await this.quote.getQuote(ticker);
    }
}
