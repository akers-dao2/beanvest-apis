import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import fetch from 'node-fetch';
import * as cheerio from 'cheerio';

@Injectable()
export class StockAvgPriceService {
    async getAvgPriceRange(ticker: string) {
        try {
            const response = await fetch(`https://finance.yahoo.com/quote/${ticker}/history?p=${ticker}`);
            const htmlText = await response.text();
            const htmlObject = cheerio.load(htmlText).parseHTML('#Col1-3-HistoricalDataTable-Proxy table tbody tr');
            return htmlObject;
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
