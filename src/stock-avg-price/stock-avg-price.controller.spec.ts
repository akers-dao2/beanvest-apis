import { Test, TestingModule } from '@nestjs/testing';
import { StockAvgPriceController } from './stock-avg-price.controller';

describe('StockAvgPrice Controller', () => {
  let controller: StockAvgPriceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockAvgPriceController],
    }).compile();

    controller = module.get<StockAvgPriceController>(StockAvgPriceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
