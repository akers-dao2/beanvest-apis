import { Controller, Get, Param } from '@nestjs/common';
import { StockAvgPriceService } from './stock-avg-price.service';

@Controller('stock-avg-price')
export class StockAvgPriceController {
    constructor(private stockAvgPrice: StockAvgPriceService) { }

    @Get(':ticker')
    async enrollSlackCommand(@Param('ticker') ticker: string) {
        await this.stockAvgPrice.getAvgPriceRange(ticker);
    }
}
