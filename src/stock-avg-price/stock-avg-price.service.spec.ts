import { Test, TestingModule } from '@nestjs/testing';
import { StockAvgPriceService } from './stock-avg-price.service';

describe('StockAvgPriceService', () => {
  let service: StockAvgPriceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StockAvgPriceService],
    }).compile();

    service = module.get<StockAvgPriceService>(StockAvgPriceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
