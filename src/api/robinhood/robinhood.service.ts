import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JsonService } from './json/json.service';
import { constants } from './constants/constants';
import * as moment from 'moment';
import fetch from 'node-fetch';
import { AccessTokenService } from './access-token/access-token.service';
import { from, interval, never, combineLatest, Subject } from 'rxjs';
import { bufferCount, mergeMap, reduce, map, concatMap, take, tap, filter, catchError } from 'rxjs/operators';
import uuid = require('uuidv4');
import { FireService } from '../../shared/services/firebase.service';
import { UserFromFirebaseService } from '../../shared/services/get-user-from-firebase.service';
import { WebClientService } from '../../shared/services/web-client.service';
import { User } from '../../slack/classes/user';

@Injectable()
export class RobinhoodService {
    constructor(
        private json: JsonService,
        private accessToken: AccessTokenService,
        private fireService: FireService,
        private userFromFirebase: UserFromFirebaseService,
        private slackWeb: WebClientService,
    ) { }

    async fundamentals(tickers: string, type: 'marketdata' | 'marketquotes') {
        try {
            const result = await this.json.get(`${constants.APIURL}/fundamentals/?symbols=${tickers}`);
            if (type) {
                const instruments = result.results.map(r => r.instrument).join(',');
                return await this[type.toLowerCase()](instruments);
            }
            return result;
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async marketdata(url) {
        url = url.replace(/https:\/api/g, 'https://api');
        const encodeUrls = encodeURIComponent(url);
        try {
            return await this.json.get(`${constants.APIURL}/marketdata/options/?instruments=${encodeUrls}`);
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async instrumentData(result) {
        const url = JSON.parse(result).url;
        try {
            return await this.json.get(`${url}`);
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async marketquotes(urls: string) {
        urls = urls.replace(/https:\/api/g, 'https://api');
        const encodeUrls = encodeURIComponent(urls);
        try {
            return await this.json.get(`${constants.APIURL}/marketdata/quotes/?instruments=${encodeUrls}`);
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async accounts(results) {
        const { username, password, token } = JSON.parse(results);
        const bearerToken = await this.accessToken.get({ username, password, token }, false);
        try {
            const fetchOptions = {
                method: 'GET',
                mode: 'no-cors',
                headers: { Authorization: 'Bearer ' + bearerToken.access_token },
            };
            const response = await fetch(`${constants.APIURL}/accounts/`, fetchOptions);
            return await response.json();
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async dayTrades(results) {
        const { username, password, token, account } = JSON.parse(results);
        const bearerToken = await this.accessToken.get({ username, password, token }, false);
        try {
            const fetchOptions = {
                method: 'GET',
                headers: { Authorization: 'Bearer ' + bearerToken.access_token },
            };
            const response = await fetch(`${constants.APIURL}/accounts/${account}/recent_day_trades/`, fetchOptions);
            return await response.json();
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async cancelOption({ username, password, token, url }) {
        const bearerToken = await this.accessToken.get({ username, password, token }, false);
        try {
            const fetchOptions = {
                method: 'POST',
                mode: 'no-cors',
                headers: { Authorization: 'Bearer ' + bearerToken.access_token },
            };
            const response = await fetch(url, fetchOptions);
            return await response.json();

        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async optionPositions({ symbol, strikePrice, username, password, token }) {
        const bearerToken = await this.accessToken.get({ username, password, token }, false);

        try {
            const fetchOptions = { mode: 'no-cors', headers: { Authorization: 'Bearer ' + bearerToken.access_token } };
            const response = await fetch(`${constants.APIURL}/options/aggregate_positions/?nonzero=true`, fetchOptions);
            const options = await response.json();
            return options.results.filter(v =>
                v.symbol === symbol.toUpperCase() &&
                v.quantity !== '0.0000' &&
                parseInt(v.legs[0].strike_price, 10) === parseInt(strikePrice, 10));
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async optionOrder(body) {
        const { username, password, token, quantity, side, price, option, account } = JSON.parse(body);

        const bearerToken = await this.accessToken.get({ username, password, token }, false);

        const form = JSON.stringify({
            quantity,
            direction: side === 'buy' ? 'debit' : 'credit',
            price,
            type: 'limit',
            account: `https://api.robinhood.com/accounts/${account}/`,
            time_in_force: 'gfd',
            trigger: 'immediate',
            legs: [
                {
                    side: side || 'buy',
                    option,
                    position_effect: side === 'buy' ? 'open' : 'close',
                    ratio_quantity: 1,
                },
            ],
            override_day_trade_checks: false,
            override_dtbp_checks: false,
            ref_id: uuid(),
        });

        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + bearerToken.access_token,
            },
            body: form,
        };
        try {
            const response = await fetch(`${constants.APIURL}/options/orders/`, fetchOptions);
            const data = await response.json();

            return data;
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async getOptions(ticker: string, date: string, data, url?: string, filterByDate = true) {
        let results: any;
        try {
            const dbAuth = await this.fireService.firestore.collection('access_token').doc('bearer').get();
            const fetchOptions = { headers: { Authorization: 'Bearer ' + dbAuth.data().token } };
            if (url === undefined) {
                // tslint:disable-next-line:max-line-length
                url = `${constants.APIURL}/options/instruments/?chain_symbol=${ticker}&type=call&tradability=tradable&state=active&expiration_date=${date}`;
            }

            const response = await fetch(url, fetchOptions);
            if (response.status === 401) {
                throw new Error(response.statusText);
            }
            const json = await response.json();
            data = data.concat(json.results);

            if (json.next !== null) {
                results = await this.getOptions(ticker, date, data, json.next, filterByDate);
            }
            const filterDataByExpDate = filterByDate ? data.filter(d => moment(d.expiration_date).isSame(date)) : data;

            return { results: filterDataByExpDate, count: filterDataByExpDate.length, ...results };
        } catch (e) {
            if (e.message === 'Unauthorized') {
                const cred = { password: process.env.PASSWORD, username: process.env.USERNAME, token: process.env.TOKEN };
                await this.accessToken.get(cred, true);
                return await this.getOptions(ticker, date, data, url, filterByDate);
            }
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async getOrders({ url, data = [], username, password, token, includeCancelItems = true }) {
        const results = {};
        const bearerToken = await this.accessToken.get({ username, password, token }, false);
        const fetchOptions = { headers: { Authorization: 'Bearer ' + bearerToken.access_token } };

        try {
            if (url === undefined) {
                url = `${constants.APIURL}/options/orders/`;
            }

            const response = await fetch(url, fetchOptions);
            const json = await response.json();
            data = data.concat(json.results);

            const filterOptionsByState = data.filter(d => {
                if (d === undefined || d.created_at === undefined) {
                    return false;
                }

                const date = d.created_at.split('T')[0];
                const jsDate = new Date();
                const currentDate = `${jsDate.getFullYear()}-${jsDate.getUTCMonth() < 10 ? '0' + (jsDate.getMonth() + 1) : jsDate.getMonth() + 1}-${jsDate.getDate()}`;
                return moment(date, 'YYYY-MM-DD').isSame(currentDate) &&
                    (includeCancelItems ? d.state === 'confirmed' || d.state === 'queued' || d.state === 'unconfirmed' : d.state !== 'cancelled');
            });
            return { results: filterOptionsByState, count: filterOptionsByState.length, ...results };
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    async addOptionDetails(options) {
        const dbAuth = await this.fireService.firestore.collection('access_token').doc('bearer').get();
        return from(options.results)
            .pipe(
                bufferCount(50),
                concatMap(r => {

                    try {
                        return fetch(`${constants.APIURL}/marketdata/options/?instruments=${r.filter(d => d).map((d: { url: string }) => d.url).join(',')}`,
                            {
                                headers: {
                                    'Authorization': 'Bearer ' + dbAuth.data().token,
                                    'Content-Type': 'application/json',
                                },
                            },
                        );

                    } catch (e) {
                        throw new HttpException(e, HttpStatus.BAD_REQUEST);
                    }
                }, (r, a) => [r, a]),
                concatMap(([_, a]) => (a as any).json(), ([r, a], j) => {
                    return (j as any).results.map((e, i) => ({ ...e, ...r[i] }));
                }),
                reduce((acc, r) => [...acc, ...r], []),
                map(r => r.sort((a, b) => {
                    const sa = parseFloat(a.strike_price);
                    const sb = parseFloat(b.strike_price);

                    if (sa > sb) {
                        return 1;
                    }
                    if (sa < sb) {
                        return -1;
                    }
                    return 0;
                })),
            )
            .toPromise();
    }

    public async addToQueue(contractURL: string, userid: string, price: string, quantity: string) {
        try {
            let id = this.fireService.firestore.collection('contractsToMonitor').doc().id;
            const querySnapShot = await this.fireService.firestore.collection('contractsToMonitor').where('userid', '==', userid).get();
            let contracts = [{ url: contractURL, price, quantity }];
            if (!!querySnapShot.docs.length) {
                id = querySnapShot.docs[0].id;
                let existingContracts = querySnapShot.docs[0].data().contracts;
                existingContracts = existingContracts.filter(c => c.url !== contractURL);
                contracts = [...existingContracts, ...contracts];
            }
            this.fireService.firestore.collection('contractsToMonitor').doc(id).set({ contracts, userid }, { merge: true });
        } catch (error) {
            throw new Error(error);
        }
    }

    private async contractMonitorScheduler() {
        const docChanges = new Subject<firebase.firestore.DocumentChange[]>();
        this.fireService.firestore.collection('contractsToMonitor').onSnapshot((snapshot) => {
            docChanges.next(snapshot.docChanges());
        });

        combineLatest(
            docChanges,
            interval(10000),
        )
            .pipe(
                mergeMap(([doc]) => doc),
                map(doc => doc.doc.data()),
                tap(console.log),
                mergeMap(data => {
                    return this.marketdata(data.contracts.map(c => c.url).join(','));
                }, (data, marketdata) => ({ data, marketdata })),
                map(r => {
                    const contracts = r.marketdata.results.filter((m, i) => m.mark_price <= parseFloat(r.data.contracts[i].price));
                    return contracts.map(c => {
                        const { quantity, price } = r.data.contracts.find(co => co.url === c.instrument);
                        return { ...c, userid: r.data.userid, option: c.instrument, quantity, price };
                    });
                }),
                filter(r => r !== undefined && r !== null),
                tap(async r => {

                    let userInfo: User;
                    let userid: string;
                    if (!!r.length) {
                        userid = r[0].userid;
                        userInfo = await this.userFromFirebase.getUser(userid);
                    }

                    r.forEach(async data => {

                        const { created_at, detail, legs, chain_symbol, direction, price, pending_quantity, processed_quantity } = await this.optionOrder({ ...userInfo, ...data, side: 'buy' });
                        if (created_at) {
                            const querySnapShot = await this.fireService.firestore.collection('contractsToMonitor').where('userid', '==', userid).get();
                            const doc = querySnapShot.docs[0].data();
                            const option = legs[0].option;
                            const contracts = doc.contracts.filter(c => c.url !== option);
                            const id = querySnapShot.docs[0].id;
                            this.fireService.firestore.collection('contractsToMonitor').doc(id).update({ contracts });
                            console.log(chain_symbol, direction, price, pending_quantity, processed_quantity)
                            const purchaseDate = moment(created_at).format('dddd, MMMM Do YYYY, h:mm:ss a');
                            const msg = `${chain_symbol} ${direction === 'debit' ? 'Purchase' : 'Sold'} on ${purchaseDate} for a total ${Number(price * pending_quantity).toLocaleString('en-US', { currency: 'USD', style: 'currency' })}`;
                            await this.slackWeb.postMessage(msg);
                        } else {
                            await this.slackWeb.postMessage(`Error: ${detail}`);
                        }

                        // await this.slackWeb.postMessage(`${data.option} Purchase at ${moment('2019-05-23T01:43:09.972547Z').format('dddd, MMMM Do YYYY, h:mm:ss a')}`);


                    });
                    console.log(`Queue Process Ran ${moment().format('dddd, MMMM Do YYYY, h:mm:ss a')}`);
                    // await this.slackWeb.postMessage(`Queue Process Ran ${moment().format('dddd, MMMM Do YYYY, h:mm:ss a')}`);


                }),
                take(1),
                catchError(e => {
                    console.error(e);
                    return never();
                }),
            )
            .subscribe();

    }
}
