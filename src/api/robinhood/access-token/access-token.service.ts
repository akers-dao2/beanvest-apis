import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import fetch from 'node-fetch';
import { constants } from '../constants/constants';
import { FireService } from '../../../shared/services/firebase.service';

@Injectable()
export class AccessTokenService {

    constructor(
        private fireService: FireService,
    ) { }

    async get({ username, password, token }, save = true) {

        const form = JSON.stringify({
            client_id: 'c82SH0WZOsabOXGP2sxqcj34FxkvfnWRZBKlBjFS',
            device_token: token,
            expires_in: '86400',
            grant_type: 'password',
            password,
            scope: 'internal',
            username,
        });

        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: form,
        };

        try {
            const response = await fetch(`${constants.APIURL}/oauth2/token/`, fetchOptions);
            const data = await response.json();

            if (save) {
                this.fireService.firestore.collection('access_token').doc('bearer').update({ token: data.access_token, lastUpdate: Date.now() });
            }

            return data;
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
