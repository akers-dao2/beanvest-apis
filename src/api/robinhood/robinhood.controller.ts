import { Controller, Get, Param, Query, UsePipes, Post, Body } from '@nestjs/common';
import { RobinhoodService } from './robinhood.service';
import { UppercasePipe } from '../../shared/pipes/uppercase.pipe';
import { FilterOptionsService } from './filter-options/filter-options.service';
import * as moment from 'moment';
import { OptionOrder } from './classes/option-order';
import { OptionPosition } from './classes/option-position';
import { Orders } from './classes/orders';
import { CancelOption } from './classes/cancel-option';
import { plainToClass } from 'class-transformer';

@Controller('robinhood')
export class RobinhoodController {
    constructor(
        private robinhood: RobinhoodService,
        private filterOptions: FilterOptionsService,
    ) { }

    @Get('fundamentals')
    @UsePipes(new UppercasePipe())
    async fundamentals(@Query('tickers') tickers: string, @Query('type') type: 'marketdata' | 'marketquotes') {
        return await this.robinhood.fundamentals(tickers, type);
    }

    @Get('marketdata')
    async marketdata(@Query('instruments') instruments: string) {
        return await this.robinhood.marketdata(instruments);
    }

    @Get('addtoqueue')
    async addToQueue(@Query('urls') urls: string, @Query('userid') userid: string, @Query('price') price: string, @Query('quantity') quantity: string) {
        return await this.robinhood.addToQueue(urls, userid, price, quantity);
    }

    @Get('marketquotes')
    async marketquotes(@Query('instruments') instruments: string) {
        return await this.robinhood.marketquotes(instruments);
    }

    @Post('accounts')
    async accounts(@Body() body) {
        return await this.robinhood.accounts(body);
    }

    @Post('instrumentdata')
    async instrumentdata(@Body() body) {
        return await this.robinhood.instrumentData(body);
    }

    @Post('daytrades')
    async dayTrades(@Body() body) {
        return await this.robinhood.dayTrades(body);
    }

    @Post('optionOrder')
    async optionOrder(@Body() body: OptionOrder) {
        body = plainToClass(OptionOrder, body);
        return await this.robinhood.optionOrder(body);
    }

    @Post('optionPositions')
    async optionPositions(@Body() body: OptionPosition) {
        body = plainToClass(OptionPosition, body);
        return await this.robinhood.optionPositions(body);
    }

    @Post('orders')
    async orders(@Body() body: Orders) {
        const orders = await this.robinhood.getOrders(body);
        const results = orders.results.sort((a, b) => {
            const aDate = moment(a.last_transaction_at);
            const bDate = moment(b.last_transaction_at);
            if (aDate.valueOf() > bDate.valueOf()) {
                return 1;
            }
            if (aDate.valueOf() < bDate.valueOf()) {
                return -1;
            }
            return 0;
        });
        return { ...orders, results };
    }

    @Get('options/:ticker/:date')
    @UsePipes(new UppercasePipe())
    async options(@Param('ticker') ticker: string, @Param('date') date: string) {
        const options = await this.robinhood.getOptions(ticker, date, []);
        return await this.robinhood.addOptionDetails(options);
    }

    @Post('canceloption')
    async cancelOption(@Body() body: CancelOption) {
        body = plainToClass(CancelOption, body);
        return await this.robinhood.cancelOption(body);
    }

    @Get('filteroptions/:ticker')
    @UsePipes(new UppercasePipe())
    async filteroptions(@Param('ticker') ticker: string) {
        const options = await this.robinhood.getOptions(ticker, undefined, [], undefined, false);
        const optionsWithDetails = await this.robinhood.addOptionDetails(options);
        const filterOption = this.filterOptions.get(optionsWithDetails);
        const optionDate = filterOption[0].expiration_date;
        const allOptionsByDate = optionsWithDetails.filter(d => moment(d.expiration_date).isSame(optionDate));
        const filterOptionByDate = filterOption.filter(d => moment(d.expiration_date).isSame(optionDate));

        return { filterOption: filterOptionByDate, allOptions: allOptionsByDate, optionDate };
    }
}
