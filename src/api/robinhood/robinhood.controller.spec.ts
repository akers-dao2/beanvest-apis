import { Test, TestingModule } from '@nestjs/testing';
import { RobinhoodController } from './robinhood.controller';

describe('Robinhood Controller', () => {
  let controller: RobinhoodController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RobinhoodController],
    }).compile();

    controller = module.get<RobinhoodController>(RobinhoodController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
