import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class FilterOptionsService {

    public get(options, use5Day = false) {
        return options
            .reduce((acc, f) => {
                let addToArray = false;
                let numOfDaysToAdd: number;
                let isSame: boolean;
                if (f.volume !== 0 &&
                    parseFloat(f.bid_price) !== 0 &&
                    parseInt(f.open_interest, 10) > 500 || f.volume > 500
                ) {
                    if (use5Day) {
                        numOfDaysToAdd = 5 - moment().day();
                        isSame = !moment(moment().add(numOfDaysToAdd, 'days').format('YYYY-MM-DD')).isSame(f.expiration_date);
                    } else {
                        numOfDaysToAdd = 72 - moment().day();
                        isSame = moment(moment().add(numOfDaysToAdd, 'days').format('YYYY-MM-DD')).isBefore(f.expiration_date);
                    }
                    addToArray = isSame;
                }
                return addToArray ? acc.concat(f) : acc;
            }, [])
            // price differential
            // .filter(f => (parseInt(f.high_price) - parseInt(f.low_price)) / parseInt(f.low_price) >= 50 )
            .sort((a, b) => {
                const aDate = moment(a.expiration_date);
                const bDate = moment(b.expiration_date);
                if (aDate.valueOf() > bDate.valueOf()) {
                    return 1;
                }
                if (aDate.valueOf() < bDate.valueOf()) {
                    return -1;
                }
                return 0;
            });
    }
}
