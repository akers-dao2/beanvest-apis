import { Injectable } from '@nestjs/common';
import fetch from 'node-fetch';
import { AccessTokenService } from '../access-token/access-token.service';
import { FireService } from '../../../shared/services/firebase.service';

@Injectable()
export class JsonService {
    constructor(
        private accessToken: AccessTokenService,
        private fireService: FireService,
    ) { }

    async get(url: string) {
        try {
            const dbAuth = await this.fireService.firestore.collection('access_token').doc('bearer').get();
            const response = await fetch(`${url}`, {
                headers: { Authorization: 'Bearer ' + dbAuth.data().token },
            });
            const results = await response.json();
            return results;
        } catch (e) {
            const cred = { password: process.env.PASSWORD, username: process.env.USERNAME, token: process.env.TOKEN };
            await this.accessToken.get(cred, true);
            return await this.get(url);
        }

    }
}
