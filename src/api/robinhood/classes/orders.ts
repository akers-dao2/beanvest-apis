export class Orders {
    public url: string;
    public data = [];
    public username: string;
    public password: string;
    public token: string;
    public bearerToken: string;
}
