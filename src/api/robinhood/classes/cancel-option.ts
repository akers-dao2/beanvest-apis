export class CancelOption {
    public username: string;
    public password: string;
    public token: string;
    public url: string;
}
