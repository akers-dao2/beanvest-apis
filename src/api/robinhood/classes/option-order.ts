export class OptionOrder {
    public username: string;
    public password: string;
    public token: string;
    public quantity: string;
    public side: string;
    public price: string;
    public option: string;
    public account: string;
}
