export class OptionPosition {
    strikePrice: number;
    symbol: string;
    password: string;
    username: string;
    token: string;
}
