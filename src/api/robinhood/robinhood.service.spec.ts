import { Test, TestingModule } from '@nestjs/testing';
import { RobinhoodService } from './robinhood.service';

describe('RobinhoodService', () => {
  let service: RobinhoodService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RobinhoodService],
    }).compile();

    service = module.get<RobinhoodService>(RobinhoodService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
