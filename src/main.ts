import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as moment from 'moment-timezone';
// tslint:disable-next-line:no-var-requires
require('dotenv').config();

async function bootstrap() {
  moment.tz.setDefault('America/New_York');
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
