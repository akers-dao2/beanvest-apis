import { Injectable } from '@nestjs/common';
import * as firebase from 'firebase';
import { User } from './classes/user';
import fetch from 'node-fetch';
import { RobinhoodService } from '../api/robinhood/robinhood.service';
import { WebClient, LogLevel, ErrorCode } from '@slack/web-api';
import * as moment from 'moment';
import { from } from 'rxjs';
import { toArray, map, concatMap } from 'rxjs/operators';
import { EnrollUser } from './classes/enrollUser';
import { EncryptionService } from '../shared/services/encryption.service';
import { FireService } from '../shared/services/firebase.service';
import { CurrencyFormatterService } from '../shared/services/currency_formatter';

@Injectable()
export class SlackService {
    private web: WebClient;
    constructor(
        private robinhoodService: RobinhoodService,
        private encryptionService: EncryptionService,
        private fireService: FireService,
        private currencyFormatter: CurrencyFormatterService,
    ) {
        this.web = new WebClient(process.env.SLACK_TOKEN, {
            logLevel: LogLevel.DEBUG,
        });
    }

    // Slack command for enrolling a user into firebase for accessing robinhood
    async enrollSlackCommand(request: any) {
        const { user_id, trigger_id } = request;
        const querySnapshot = await this.fireService.firestore.collection('users').where('id', '==', user_id).get();
        let user: Partial<User> = { password: '' };
        let id: string;
        let username: string;
        let account: string;

        if (querySnapshot.docs[0]) {
            user = querySnapshot.docs[0].data() as User;
            id = querySnapshot.docs[0].id;
            username = this.decryptedValue(user.username);
            account = this.decryptedValue(user.account);
        }

        try {
            return await this.web.dialog.open({
                trigger_id,
                dialog: {
                    title: `Beanvest ${username ? 'Edit User' : 'User Enrollment'}`,
                    callback_id: 'beanvest_user_enrollment',
                    submit_label: username ? 'Update' : 'Enroll',
                    state: id,
                    elements: [
                        {
                            label: 'Email or Username',
                            name: 'username',
                            type: 'text',
                            subtype: 'email',
                            value: username,
                        },
                        {
                            label: 'Password',
                            name: 'password',
                            type: 'text',
                            value: user.password.length > 30 ? 'Encrypted Password' : '',

                        },
                        {
                            label: 'Account Number',
                            name: 'account',
                            type: 'text',
                            value: account,

                        },
                        {
                            label: 'Hold Amount',
                            name: 'holdAmount',
                            type: 'text',
                            value: user.holdAmount,

                        },
                        {
                            label: 'Number of Trades Per Day',
                            name: 'tradesPerDay',
                            type: 'text',
                            value: user.tradesPerDay,

                        },
                        {
                            label: 'Device Token',
                            name: 'token',
                            type: 'textarea',
                            hint: 'Pull your device_token via the network panel, filter by token and look for the POST call',
                            value: user.token,
                        },
                    ],
                },
            });
        } catch (error) {
            if (error.code === ErrorCode.PlatformError) {
                throw new Error(error.data);
            } else {
                throw new Error(error);
            }
        }

    }

    // Handles the slack interactions request from the enrollSlackCommand
    public async enrollUser(state: string, submission: EnrollUser, user: Partial<User>, url: string) {
        let ref = this.fireService.firestore.collection('users').doc().id;
        let msg = 'Enrollment Successful';
        if (state !== '') {
            ref = state;
            if (submission.password === 'Encrypted Password') {
                const userFromFirebase = await this.getUser(user.id);
                submission.password = userFromFirebase.password;
            }
            msg = 'Updated Enrollment Successful';
        }

        const password = this.updateEncryptedValues(submission.password);
        const username = this.updateEncryptedValues(submission.username);
        const account = this.updateEncryptedValues(submission.account);
        this.fireService.firestore.collection('users').doc(ref).set({ ...submission, ...user, password, username, account });
        this.setSlackMessage(msg, url);
    }

    // Slack command for adding play statuses
    async playStatusSlackCommand(request: any) {
        const { trigger_id } = request;
        try {
            return await this.web.dialog.open(
                {
                    trigger_id,
                    dialog: {
                        title: `Beanvest Play Status`,
                        callback_id: 'play_status',
                        elements: [
                            {
                                label: `Type of Play`,
                                name: 'typeOfPlay',
                                type: 'select',
                                options: [
                                    {
                                        label: 'Successful',
                                        value: 'successful',
                                    },
                                    {
                                        label: 'Missed',
                                        value: 'missed',
                                    },
                                    {
                                        label: 'Failed',
                                        value: 'failed',
                                    },
                                ],
                            },
                            {
                                label: 'Stock Name',
                                name: 'stockName',
                                type: 'text',
                            },
                            {
                                label: 'Buy Amount',
                                name: 'buy',
                                type: 'text',
                                subtype: 'number',

                            },
                            {
                                label: 'Sell Amount',
                                name: 'sell',
                                type: 'text',
                                subtype: 'number',

                            },
                            {
                                label: '# of Contracts',
                                name: 'numOfcontracts',
                                type: 'text',
                                subtype: 'number',

                            },
                        ],

                    },
                },
            );

        } catch (error) {
            if (error.code === ErrorCode.PlatformError) {
                throw new Error(error.data);
            } else {
                throw new Error(error);
            }
        }

    }

    // Handles the slack interactions request from the playStatusSlackCommand
    public playStatus(submission, user: User, url: string) {
        let { buy, sell, numOfcontracts } = submission;
        buy = parseFloat(buy);
        sell = parseFloat(sell);
        numOfcontracts = parseFloat(numOfcontracts);
        const percentIncrease = (sell - buy) / buy * 100;
        const date = new Date().valueOf();
        this.fireService.firestore.collection('plays').add({ ...submission, ...user, percentIncrease, buy, sell, numOfcontracts, date });
        this.setSlackMessage('Play Status Added', url);
    }

    // Handles the loading of the options drop down in Buy Options dialog
    async optionsDynamicLoad(d) {
        try {
            const { state, name } = JSON.parse(d.payload);

            const resp = await fetch(state);
            const data = await resp.json();

            const { limitPrices, limitPriceDoubles } = data;
            let options = [];
            switch (name) {
                case 'ntype':
                    options = limitPrices.slice(0, 100).map(l => {
                        return {
                            label: `Strike Price: ${Number(l.strikePrice).toLocaleString('en-US', { currency: 'USD', style: 'currency' })}, Limit Price: ${Number(l.limitPrice).toLocaleString('us', { currency: 'USD', style: 'currency' })}`.substring(0, 75),
                            value: l.url.split('/')[5] + '/'.substring(0, 75),
                        };
                    });
                    break;
                case 'dtype':
                    options = limitPriceDoubles.slice(0, 100).map(l => {
                        return {
                            label: `Strike Price: ${Number(l.strikePrice).toLocaleString('en-US', { currency: 'USD', style: 'currency' })}, Limit Price: ${Number(l.limitPrice).toLocaleString('us', { currency: 'USD', style: 'currency' })}`.substring(0, 75),
                            value: l.url.split('/')[5] + '/'.substring(0, 75),
                        };
                    });
                    break;
            }

            return {
                options,
            };

        } catch (error) {
            throw new Error(error.data);
        }

    }

    // Slack command for purchasing options via Robinhood through slack
    async limitOptionSlackCommand(payload, data) {
        try {
            const { trigger_id } = payload;
            const { earnings, play, news, isEarningPlay, url } = data;
            const resp = await fetch(url.replace('/option?', '/option/nolimit?'));
            const { callDate, averageRange, supportPrice } = await resp.json();

            return this.web.dialog.open({
                trigger_id,
                dialog: {
                    title: `Limit Options Prices`,
                    callback_id: 'beanvest_limit_options_price',
                    state: url,
                    elements: [
                        {
                            label: `Normal Play ${news <= 0 && !isEarningPlay ? '(Recommend)' : ''}`,
                            name: 'ntype',
                            type: 'select',
                            data_source: 'external',
                            optional: true,
                        },
                        {
                            label: `Double Play ${news >= 0 || isEarningPlay ? '(Recommend)' : ''}`,
                            name: 'dtype',
                            type: 'select',
                            data_source: 'external',
                            optional: true,
                        },
                        {
                            label: 'Price',
                            name: 'price',
                            type: 'text',
                            subtype: 'number',
                        },
                        {
                            label: 'Additional information',
                            name: 'comment',
                            type: 'textarea',
                            value: `Play: ${play}\nEarnings Date: ${earnings}\nNews: ${news}\nSupport Price: ${supportPrice}\nAverage Range: ${averageRange}\nCall Date: ${callDate}\n`,
                        },
                    ],

                },
            });

        } catch (error) {
            if (error.code === ErrorCode.PlatformError) {
                throw new Error(error.data);
            } else {
                throw new Error(error);
            }
        }

    }

    // Handles the slack interactions request from the limitOptionSlackCommand
    public async limitOption(submission, user, responseURL: string) {
        const { id } = user;
        const { ntype, dtype } = submission;
        const querySnapshot = await this.fireService.firestore.collection('users').where('id', '==', id).get();
        if (querySnapshot.docs[0]) {
            user = querySnapshot.docs[0].data() as User;
            const result = await this.robinhoodService.optionOrder(JSON.stringify({ ...submission, option: `https://api.robinhood.com/options/instruments/${ntype || dtype}`, side: 'buy', quantity: 1, ...user }));
            const { created_at } = result;
            this.setSlackMessage(`Purchase at ${created_at}`, responseURL);

        }
    }

    public getValueFromRegex(msg: string, reg: RegExp) {
        const result = reg.exec(msg);
        return result[1];
    }

    public async setSlackMessage(msg: string, url, blocks?) {
        blocks = blocks || [
            {
                type: 'section',
                text: {
                    type: 'mrkdwn',
                    text: `*${msg}*`,
                },
            },
        ];

        const form = JSON.stringify({
            blocks,
            response_type: 'in_ephemeral',
        });

        this.httpFetch(form, url);
    }

    // Slack command for purchasing options manually via Robinhood through slack
    public async buyOptionSlackCommand(payload) {
        const { user_id } = payload;
        const user = await this.getUser(user_id);

        const { trigger_id } = payload;
        try {
            return this.web.dialog.open({
                trigger_id,
                dialog: {
                    title: 'Buy Option',
                    callback_id: 'beanvest_buy_option',
                    submit_label: 'Buy',
                    state: user.id,
                    elements: [
                        {
                            label: 'Quantity',
                            name: 'quantity',
                            type: 'text',
                            subtype: 'number',
                        },
                        {
                            label: 'Price',
                            name: 'price',
                            type: 'text',
                            subtype: 'number',

                        },
                        {
                            label: 'Option URL',
                            name: 'option',
                            type: 'text',
                        },
                        {
                            label: 'Type',
                            name: 'side',
                            type: 'select',
                            value: 'buy',
                            options: [
                                {
                                    label: 'Buy',
                                    value: 'buy',
                                },
                                {
                                    label: 'Sell',
                                    value: 'sell',
                                },
                            ],
                        },
                    ],

                },
            });
        } catch (error) {
            if (error.code === ErrorCode.PlatformError) {
                throw new Error(error.data);
            } else {
                throw new Error(error);
            }
        }
    }

    // Handles the slack interactions request from the buyOptionSlackCommand
    public async executeOptionOrder(submission, state, responseURL) {

        try {
            this.setSlackMessage(`Processing...`, responseURL);
            const user = await this.getUser(state);
            const result = await this.robinhoodService.optionOrder(JSON.stringify({ ...submission, ...user }));

            const { created_at, detail } = result;
            if (created_at) {
                this.setSlackMessage(`Purchase at ${moment(created_at).format('dddd, MMMM Do YYYY, h:mm:ss a')}`, responseURL);
            } else {
                this.setSlackMessage(`Error: ${detail}`, responseURL);
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    async optionDetailsSlackCommand({ text, response_url }) {
        const data = await this.robinhoodService.marketdata(text);
        const { ask_price, ask_size, bid_price, bid_size, last_trade_price, low_price, mark_price, high_price, instrument } = data.results[0];
        const { chain_symbol, strike_price, expiration_date } = await this.robinhoodService.instrumentData(JSON.stringify({url: instrument}));

        const blocks = [
            {
                type: 'section',
                text: {
                    type: 'mrkdwn',
                    text: `*Option INFO:* ${chain_symbol} ${this.currencyFormatter.format(strike_price)} Expiring: ${expiration_date}`,
                },
            },
            {
                type: 'divider',
            },
            {
                type: 'section',
                text: {
                    type: 'mrkdwn',
                    // tslint:disable-next-line:max-line-length
                    text: `*Ask Price/Size:* ${this.currencyFormatter.format(ask_price)} / ${ask_size}\n*Bid Price/Size:* ${this.currencyFormatter.format(bid_price)} / ${bid_size}\n*Current Price:* ${this.currencyFormatter.format(mark_price)}\n*Last Trade Price:* ${this.currencyFormatter.format(last_trade_price)}\n*Low Price:* ${this.currencyFormatter.format(low_price)}\n*High Price:* ${this.currencyFormatter.format(high_price)}`,
                },
            },
            {
                type: 'context',
                elements: [
                    {
                        type: 'mrkdwn',
                        text: `*as of:* ${moment().format('dddd, MMMM Do YYYY, h:mm:ss a')}`,
                    },
                ],
            },

        ];
        this.setSlackMessage(undefined, response_url, blocks);
    }

    // Slack command for viewing account details from Robinhood in Slack
    async accountsSlackCommand({ user_id, response_url }) {
        const user = await this.getUser(user_id);
        const result = await this.robinhoodService.accounts(user);
        const { margin_balances, account_number } = result.results[0];
        const [dayTrades] = await Promise.all(
            [
                this.robinhoodService.dayTrades({ ...user, accountNumber: account_number }),
            ],
        );
        const numberofDayTrades = dayTrades.option_day_trades.length + dayTrades.equity_day_trades.length;
        const amountWithHold = margin_balances.day_trade_buying_power - parseFloat(user.holdAmount);
        const blocks = [
            {
                type: 'section',
                text: {
                    type: 'mrkdwn',
                    text: `*ACCOUNT INFO:*`,
                },
            },
            {
                type: 'divider',
            },
            {
                type: 'section',
                text: {
                    type: 'mrkdwn',
                    // tslint:disable-next-line:max-line-length
                    text: `*Original Buy Power:* ${this.currencyFormatter.format(margin_balances.day_trade_buying_power)}\n*Buy Power Minus Hold Amount:* ${this.currencyFormatter.format(amountWithHold)}\n*Number of Day Trades Used:* ${numberofDayTrades}\n*Account:* ${account_number}`,
                },
            },
            {
                type: 'context',
                elements: [
                    {
                        type: 'mrkdwn',
                        text: `*as of:* ${moment().format('dddd, MMMM Do YYYY, h:mm:ss a')}`,
                    },
                ],
            },
        ];
        this.setSlackMessage(undefined, response_url, blocks);
    }

    // Slack command for cancelling option orders in Robinhood through Slack
    async cancelOptionOrdersSlackCommand(request) {
        const { trigger_id, user_id } = request;
        try {
            const userInfo = await this.getUser(user_id);
            const orders = await this.robinhoodService.getOrders({ ...userInfo, url: undefined });
            const cancelURLs = [];
            const pendingOrders = await from(orders.results)
                .pipe(
                    concatMap(order => {
                        const { cancel_url, legs } = order;
                        const option = legs[0].option;
                        cancelURLs.push(cancel_url);
                        return Promise.all([
                            this.robinhoodService.instrumentData(JSON.stringify({url: option})),
                            this.robinhoodService.marketdata(option),
                        ]);
                    }, (o, i) => ({ o, i })),
                    map(({ o, i }, index) => {
                        const { strike_price, type, expiration_date, price, quantity } = i[0];
                        const { ask_price, ask_size, bid_price, bid_size, last_trade_price, low_price, mark_price, high_price, instrument } = i[1];
                        const { chain_symbol } = o;
                        return `#${index} ${chain_symbol} ${type.charAt(0).toUpperCase() + type.slice(1)} ${Number(strike_price).toLocaleString('en-US', { currency: 'USD', style: 'currency' })} ${moment(expiration_date).format('MM/DD')}`;
                    }),
                    toArray(),
                )
                .toPromise();

            return await this.web.dialog.open(
                {
                    trigger_id,
                    dialog: {
                        title: `Beanvest Cancel Orders`,
                        callback_id: 'cancel_option_order',
                        state: JSON.stringify(cancelURLs),
                        elements: [
                            {
                                label: 'Additional information',
                                name: 'comment',
                                type: 'textarea',
                                value: pendingOrders.join('\n'),
                            },
                        ],

                    },
                },
            );

        } catch (error) {
            if (error.code === ErrorCode.PlatformError) {
                throw new Error(error.data);
            } else {
                throw new Error(error);
            }
        }

    }

    // Handles the slack interactions request from the cancelOptionOrdersCommand
    async cancelOptionOrders(submission, state: string, responseURL: string, user: User) {
        const userData = await this.getUser(user.id);
        const regex = /(?!#)\d/;
        const cancelOptions = submission.comment.split('\n').filter(d => d !== '').map(d => parseInt(regex.exec(d)[0], 10));
        const cancelOptionURLs = JSON.parse(state).filter((_, i) => cancelOptions.includes(i));
        cancelOptionURLs.forEach(async url => {
            const result = await this.robinhoodService.cancelOption({ url, ...userData });
            this.setSlackMessage(JSON.stringify(result), responseURL);
        });
    }

    /**
     * ********************************
     *              Private
     * ********************************
     */

    private async httpFetch(form, url = 'https://slack.com/api/dialog.open') {
        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer xoxp-36273005847-36273678165-644442160100-b63398830e6a889a52db37e49e85409e',
            },
            body: form,
        };
        try {
            return await fetch(url, fetchOptions);
        } catch (e) {
            throw new Error(e);
        }
    }

    private async getUser(id: string) {
        try {
            const querySnapshot = await this.fireService.firestore.collection('users').where('id', '==', id).get();
            const user = querySnapshot.docs[0].data() as User;
            const username = this.decryptedValue(user.username);
            const account = this.decryptedValue(user.account);
            const password = this.decryptedValue(user.password);
            return { ...user, username, account, password };
        } catch (error) {
            throw new Error(error);
        }
    }

    private decryptedValue(value: string) {
        if (value.length > 30) {
            value = this.encryptionService.decrypt(value);
        }

        return value;
    }

    private updateEncryptedValues(value: string) {
        if (value.length > 30) {
            value = this.encryptionService.decrypt(value);
        }

        return this.encryptionService.encrypt(value);
    }
}
