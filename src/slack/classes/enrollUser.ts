export class EnrollUser {
    public username: string;
    public password: string;
    public account: string;
    public deviceToken: string;
}
