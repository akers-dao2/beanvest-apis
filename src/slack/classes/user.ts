export class User {
    username: string;
    id: string;
    password: string;
    token: string;
    account: string;
    accountNumber: string;
    holdAmount: string;
    tradesPerDay: string;
}
