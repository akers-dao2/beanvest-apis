import { Controller, Post, Body } from '@nestjs/common';
import { SlackService } from './slack.service';

@Controller('slack')
export class SlackController {
    constructor(
        private slackService: SlackService,
    ) { }

    @Post('enrollSlackCommand')
    async enrollSlackCommand(@Body() body) {
        await this.slackService.enrollSlackCommand(body);
    }

    @Post('buyOptionSlackCommand')
    async buyOptionSlackCommand(@Body() body) {
        await this.slackService.buyOptionSlackCommand(body);
    }

    @Post('playStatusSlackCommand')
    async successfulPlaySlackCommand(@Body() body) {
        await this.slackService.playStatusSlackCommand(body);
    }

    @Post('cancelOptionOrdersCommand')
    async cancelOptionOrdersCommand(@Body() body) {
        await this.slackService.cancelOptionOrdersSlackCommand(body);
    }

    @Post('accountsSlackCommand')
    async accountsSlackCommand(@Body() body) {
        await this.slackService.accountsSlackCommand(body);
    }

    @Post('optionDetailsSlackCommand')
    async optionDetailsSlackCommand(@Body() body) {
        await this.slackService.optionDetailsSlackCommand(body);
    }

    @Post('optionsdynamicload')
    async optionsdynamicload(@Body() body) {
        await this.slackService.optionsDynamicLoad(body);
    }

    @Post('interactions')
    async interactions(@Body() body) {
        const payload = JSON.parse(body.payload);
        const { response_url, submission, user, state, callback_id, message, trigger_id } = payload;
        switch (callback_id) {
            case 'beanvest_buy_option':
                await this.slackService.executeOptionOrder(submission, state, response_url);
                break;

            case 'beanvest_user_enrollment':
                this.slackService.enrollUser(state, submission, user, response_url);
                break;

            case 'buy':
                const regEx = /http:\/\/[\w-./?=&;]+/g;
                const play = this.slackService.getValueFromRegex(message.text, /Play:\s(.*)/g);
                const symbol = this.slackService.getValueFromRegex(message.text, /Symbol:\s(.*)/g);
                const earnings = this.slackService.getValueFromRegex(message.text, /Last Earnings:\s(.*)/g);
                const news = this.slackService.getValueFromRegex(message.text, /News Sentiment Score:\s(.*)/g);
                const isEarningPlay = this.slackService.getValueFromRegex(message.text, /Is Earnings Play:\s(.*)/g);
                const url = regEx.exec(message.text)[0].replace(/amp;/g, '');
                const data = {};

                this.slackService.limitOptionSlackCommand(payload, { ...data, earnings, symbol, play, news, isEarningPlay, url });
                break;

            case 'beanvest_limit_options_price':
                this.slackService.limitOption(submission, user, response_url);
                break;

            case 'play_status':
                await this.slackService.playStatus(submission, user, response_url);
                break;

            case 'cancel_option_order':
                await this.slackService.cancelOptionOrders(submission, state, response_url, user);
                break;

        }
    }
}
