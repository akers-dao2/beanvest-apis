import { Injectable } from '@nestjs/common';
import * as firebase from 'firebase';

@Injectable()
export class FireService {
    private fdb: firebase.firestore.Firestore;
    // Initialize Cloud Firestore through Firebase
    constructor() {
        firebase.initializeApp({
            apiKey: process.env.APIKEY,
            authDomain: process.env.AUTHDOMAIN,
            projectId: process.env.PROJECTID,
        });

        this.fdb = firebase.firestore();

    }

    public get firestore() {
        return this.fdb;
    }
}
