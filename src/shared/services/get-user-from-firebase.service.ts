import { Injectable } from '@nestjs/common';
import { FireService } from './firebase.service';
import { User } from '../../slack/classes/user';
import { EncryptionService } from './encryption.service';

@Injectable()
export class UserFromFirebaseService {
    constructor(
        private fireService: FireService,
        private encryptionService: EncryptionService,
    ) { }

    public async getUser(id: string) {
        try {
            const querySnapshot = await this.fireService.firestore.collection('users').where('id', '==', id).get();
            const user = querySnapshot.docs[0].data() as User;
            const username = this.decryptedValue(user.username);
            const account = this.decryptedValue(user.account);
            const password = this.decryptedValue(user.password);
            return { ...user, username, account, password };
        } catch (error) {
            throw new Error(error);
        }
    }

    private decryptedValue(value: string) {
        if (value.length > 30) {
            value = this.encryptionService.decrypt(value);
        }

        return value;
    }

}
