import { Injectable } from '@nestjs/common';

@Injectable()
export class CurrencyFormatterService {
    public format(value: any) {
        return Number(value).toLocaleString('en-US', { currency: 'USD', style: 'currency' });
    }
}
