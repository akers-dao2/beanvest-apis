import { Injectable } from '@nestjs/common';
import { WebClient, LogLevel } from '@slack/web-api';

@Injectable()
export class WebClientService {
    private web: WebClient;
    constructor() {
        this.web = new WebClient(process.env.SLACK_TOKEN, {
            logLevel: LogLevel.DEBUG,
        });
    }

    public get client() {
        return this.web;
    }

    public async postMessage(text: string) {
        try {
            await this.web.chat.postMessage({ channel: 'D1KEUCZ1Q', text });
        } catch (error) {
            throw new Error(error);
        }
    }
}
