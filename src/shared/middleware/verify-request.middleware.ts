import * as crypto from 'crypto';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class VerifyRequest implements NestMiddleware {
    use(req: Request, res: Response, next: () => void) {
        const hmac = crypto.createHmac('sha256', '9f798e3c950fa41ed8507314e2e5e9e0');
        const timestamp = req.get('X-Slack-Request-Timestamp');
        const concatenateValue = `v0:${timestamp}:${req.body}`;
        hmac.update(concatenateValue);
        next();
    }
}
